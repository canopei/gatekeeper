package main

import (
	"context"
	"io"
	"net/http"

	"bitbucket.org/canopei/golibs"
	cHttp "bitbucket.org/canopei/golibs/http"
	"github.com/Sirupsen/logrus"
	"github.com/gorilla/handlers"
)

// LoggingMiddleware is a logging HTTP middlware
type LoggingMiddleware struct {
	Logger *logrus.Entry
	CtxKey ContextKey
}

// NewLoggingMiddlware creates a new instance of LoggingMiddleware
func NewLoggingMiddlware(logger *logrus.Entry, ctxKey ContextKey) *LoggingMiddleware {
	return &LoggingMiddleware{
		Logger: logger,
		CtxKey: ctxKey,
	}
}

func (lmw *LoggingMiddleware) ServeHTTP(res http.ResponseWriter, req *http.Request, next http.HandlerFunc) {
	requestLog := logrus.New()
	requestLog.Out = lmw.Logger.Logger.Out
	requestLog.Level = lmw.Logger.Logger.Level
	requestLog.Formatter = lmw.Logger.Logger.Formatter
	requestLog.Hooks = lmw.Logger.Logger.Hooks

	fields := lmw.Logger.Data
	fields[golibs.LogUserIPKeyName] = cHttp.GetIPAdress(req)

	requestID := req.Header.Get(cHttp.DefaultRequestIDHeaderKey)
	if requestID != "" {
		fields[golibs.LogRequestIDKeyName] = requestID
		req.Header.Set(golibs.HeaderGrpcRequestIDName, requestID)
	}

	requestLogger := requestLog.WithFields(fields)
	req = req.WithContext(context.WithValue(req.Context(), lmw.CtxKey, requestLogger))

	handlers.CombinedLoggingHandler(io.Writer(requestLogger.Writer()), next).ServeHTTP(res, req)
}
