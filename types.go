package main

// ContextKey holds an int context key
type ContextKey int

const (
	// ServiceContextKey is the context key for the service
	ServiceContextKey ContextKey = iota

	// LoggerContextKey is the context key for the logger
	LoggerContextKey
)
