package main

// Service holds a service configuration
type Service struct {
	Name    string `toml:"name"`
	Backend string `toml:"backend_addr"`
}

// ServiceDirectory holds the service directory
type ServiceDirectory map[string]*Service
