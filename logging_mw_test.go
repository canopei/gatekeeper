package main

import (
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"

	"bitbucket.org/canopei/golibs"
	"github.com/Sirupsen/logrus"
	"github.com/stretchr/testify/assert"
)

func TestServeHTTPSimple(t *testing.T) {
	assert := assert.New(t)

	logger := logrus.WithFields(logrus.Fields{})
	// Mute the logger
	logger.Logger.Out = ioutil.Discard

	mw := NewLoggingMiddlware(logger, 1)

	// Used to ensure that next is called
	nextCalled := false
	next := func(res http.ResponseWriter, req *http.Request) {
		nextCalled = true
	}

	req, err := http.NewRequest(http.MethodGet, "dummy", nil)
	assert.NoError(err)
	rec := httptest.NewRecorder()

	mw.ServeHTTP(rec, req, next)

	assert.True(nextCalled)
}

func TestServeHTTPComplete(t *testing.T) {
	assert := assert.New(t)

	loggerContextKey := LoggerContextKey
	logger := logrus.WithFields(logrus.Fields{})
	// Mute the logger
	logger.Logger.Out = ioutil.Discard

	mw := NewLoggingMiddlware(logger, loggerContextKey)

	// Used to ensure that next is called
	nextCalled := false
	next := func(res http.ResponseWriter, req *http.Request) {
		nextCalled = true

		// Get the request logger from the context; it should be there!
		contextLogger := req.Context().Value(loggerContextKey)
		assert.NotNil(contextLogger)

		requestLogger := contextLogger.(*logrus.Entry)
		assert.NotNil(requestLogger)
		// Check that the request ID is also passed correctly
		assert.Equal("reqid", requestLogger.Data[golibs.LogRequestIDKeyName])
		assert.Equal("1.1.1.1", requestLogger.Data[golibs.LogUserIPKeyName])
	}

	req, err := http.NewRequest(http.MethodGet, "dummy", nil)
	assert.NoError(err)
	req.Header = http.Header{
		"X-Request-Id": []string{"reqid"},
	}
	req.RemoteAddr = "1.1.1.1"

	rec := httptest.NewRecorder()

	mw.ServeHTTP(rec, req, next)

	assert.True(nextCalled)
}
