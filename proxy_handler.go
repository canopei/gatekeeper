package main

import (
	"bytes"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"strings"

	"bitbucket.org/canopei/golibs"
	cSlices "bitbucket.org/canopei/golibs/slices"
	"github.com/Sirupsen/logrus"
)

// HTTPClient is an interface for our HTTP client
type HTTPClient interface {
	Do(req *http.Request) (*http.Response, error)
}

// ProxyHandler forwards the request to another service
type ProxyHandler struct {
	Logger *logrus.Entry
	Client HTTPClient
}

// NewProxyHandler creates a new instance of ProxyHandler
func NewProxyHandler(logger *logrus.Entry, client HTTPClient) *ProxyHandler {
	return &ProxyHandler{
		Logger: logger,
		Client: client,
	}
}

// Handle forwards the request to the service
func (ph *ProxyHandler) Handle(res http.ResponseWriter, req *http.Request) {
	var err error

	contextService := req.Context().Value(ServiceContextKey)
	if contextService == nil {
		ph.Logger.Errorf("The Service was not found in the context.")
		res.WriteHeader(http.StatusBadRequest)
		return
	}
	service := contextService.(*Service)
	destination := fmt.Sprintf("http://%s%s", service.Backend, req.URL.RequestURI())

	ph.Logger.Debugf("Proxying to service '%s': %s", service.Name, destination)

	b := []byte("")
	if req.Body != nil {
		b, err = ioutil.ReadAll(req.Body)
		if err != nil {
			ph.Logger.Errorf("Could not read the request body: %v", err)
			res.WriteHeader(http.StatusBadRequest)
			return
		}
		defer req.Body.Close()
	}

	internalReq, err := http.NewRequest(req.Method, destination, bytes.NewReader(b))
	if err != nil {
		ph.Logger.Errorf("Unable to create the internal request: %v", err)
		res.WriteHeader(http.StatusInternalServerError)
		return
	}

	// Pass all the external request headers to the internal request
	skipExternalHeaders := []string{}
	for k, v := range req.Header {
		if !cSlices.Scontains(skipExternalHeaders, strings.ToLower(k)) {
			internalReq.Header.Add(k, strings.Join(v, ","))
		}
	}

	internalRes, err := ph.Client.Do(internalReq)
	if err != nil {
		ph.Logger.Errorf("Unable to query backend: %v", err)
		res.WriteHeader(http.StatusBadGateway)
		return
	}
	if internalRes.Body != nil {
		defer internalRes.Body.Close()
	}

	// Copy the response headers
	skipInternalHeaders := []string{strings.ToLower(golibs.HeaderRequestIDName)}
	for k, v := range internalRes.Header {
		if !cSlices.Scontains(skipInternalHeaders, strings.ToLower(k)) {
			res.Header().Add(k, strings.Join(v, ","))
		}
	}

	// Pass the HTTP status code
	res.WriteHeader(internalRes.StatusCode)

	// Copy the respone body
	if internalRes.Body != nil {
		io.Copy(res, internalRes.Body)
	}
}
