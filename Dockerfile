FROM alpine:latest
MAINTAINER Aris Buzachis <aris@canopei.com>

RUN apk add --update --no-cache ca-certificates

RUN mkdir -p /var/sng/bin
COPY build/ /var/sng/bin/
RUN chmod +x /var/sng/bin/gatekeeper

EXPOSE 8080

CMD cd /var/sng/bin && ./gatekeeper