package main

import (
	"bitbucket.org/canopei/golibs/logging"
	"github.com/BurntSushi/toml"
)

// ServiceConfig holds the service general configuration
type ServiceConfig struct {
	Name         string
	Env          string
	Port         int16  `toml:"port"`
	ExternalApex string `toml:"external_apex"`
}

// Config holds the entire configuration
type Config struct {
	Service  ServiceConfig          `toml:"service"`
	Logstash logging.LogstashConfig `toml:"logstash"`
	Services ServiceDirectory       `toml:"services"`
}

// LoadConfig loads the configuration from the given filepath
func LoadConfig(filePath string) (*Config, error) {
	var conf Config
	_, err := toml.DecodeFile(filePath, &conf)

	return &conf, err
}
