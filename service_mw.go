package main

import (
	"context"
	"fmt"
	"net/http"
	"strings"

	"github.com/Sirupsen/logrus"
)

// ServiceMiddleware implements the services middleware
type ServiceMiddleware struct {
	ExternalApex      string
	Services          ServiceDirectory
	Logger            *logrus.Entry
	LoggerContextKey  ContextKey
	ServiceContextKey ContextKey
}

// NewServiceMiddleware creates a new instance of ServiceMiddleware
func NewServiceMiddleware(logger *logrus.Entry, externalApex string, services ServiceDirectory, loggerContextKey ContextKey, serviceContextKey ContextKey) *ServiceMiddleware {
	return &ServiceMiddleware{
		ExternalApex:      externalApex,
		Services:          services,
		Logger:            logger,
		LoggerContextKey:  loggerContextKey,
		ServiceContextKey: serviceContextKey,
	}
}

// ServeHTTP our http middleware handler
func (svc *ServiceMiddleware) ServeHTTP(res http.ResponseWriter, req *http.Request, next http.HandlerFunc) {
	requestLogger := svc.Logger
	contextLogger := req.Context().Value(svc.LoggerContextKey)
	if contextLogger != nil {
		requestLogger = contextLogger.(*logrus.Entry)
	}

	subdomain, err := GetSubdomainFromHost(req.Host, svc.ExternalApex)
	if err != nil {
		requestLogger.Debugf("Cannot find subdomain in %s with apex %s.", req.Host, svc.ExternalApex)
	}
	requestLogger.Debugf("Found sudomain: %s", subdomain)

	service, ok := svc.Services[subdomain]
	if !ok {
		requestLogger.Errorf("Cannot find service '%s'.", subdomain)
	} else {
		ctx := req.Context()
		ctx = context.WithValue(ctx, svc.ServiceContextKey, service)
		req = req.WithContext(ctx)
	}

	next(res, req)
}

// GetSubdomainFromHost retrieves the subdomain from the Host using the external apex (safest)
func GetSubdomainFromHost(host string, externalApex string) (string, error) {
	host = strings.ToLower(host)
	externalApex = "." + strings.ToLower(externalApex)

	// remove the :port
	colonIndex := strings.Index(host, ":")
	if colonIndex > -1 {
		host = host[:colonIndex]
	}

	// sanitiy check
	if len(host) <= len(externalApex) {
		return "", fmt.Errorf("host is less than external apex length")
	}

	subdomainLength := len(host) - len(externalApex)
	if host[subdomainLength:] != externalApex {
		return "", fmt.Errorf("host does not contain the root domain")
	}

	return host[:subdomainLength], nil
}
