package main

import (
	"bytes"
	"context"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"

	"fmt"

	"bitbucket.org/canopei/golibs"
	"github.com/Sirupsen/logrus"
	"github.com/stretchr/testify/assert"
)

func TestProxyHandlerNoService(t *testing.T) {
	assert := assert.New(t)

	logger := logrus.WithFields(logrus.Fields{})
	// Mute the logger
	logger.Logger.Out = ioutil.Discard

	proxyHandler := NewProxyHandler(logger, getMockClient(func(*http.Request) (*http.Response, error) {
		return &http.Response{}, nil
	}))
	req, err := http.NewRequest(http.MethodGet, "dummy", nil)
	assert.NoError(err)
	rec := httptest.NewRecorder()

	proxyHandler.Handle(rec, req)

	// Check for bad request
	assert.Equal(http.StatusBadRequest, rec.Result().StatusCode)
}

func TestProxyHandlerWithService(t *testing.T) {
	assert := assert.New(t)

	logger := logrus.WithFields(logrus.Fields{})
	// Mute the logger
	logger.Logger.Out = ioutil.Discard

	bodyTestText := "foo bar"

	proxyHandler := NewProxyHandler(logger, getMockClient(func(req *http.Request) (*http.Response, error) {
		assert.Contains(req.Header.Get("Test-Header"), "foo")
		assert.Contains(req.Header.Get("Test-Header"), "bar")
		// Check the Authorization header
		assert.Equal("12345foo", req.Header.Get("Authorization"))
		// Check the Request ID header
		assert.Equal("reqid", req.Header.Get(golibs.HeaderRequestIDName))

		res := &http.Response{
			Body: ioutil.NopCloser(bytes.NewBufferString(bodyTestText)),
		}
		res.Header = http.Header{
			"TestH": []string{"foo"},
			// We need to check that this doesn't get to the client
			golibs.HeaderRequestIDName: []string{"foo"},
		}

		return res, nil
	}))
	req, err := http.NewRequest(http.MethodGet, "dummy", nil)
	req.Header = http.Header{
		// Check that these headers are being forwarded to the internal request
		"Test-Header":              []string{"foo", "bar"},
		"Authorization":            []string{"12345foo"},
		golibs.HeaderRequestIDName: []string{"reqid"},
	}
	ctx := req.Context()
	ctx = context.WithValue(ctx, ServiceContextKey, &Service{
		Name:    "foo",
		Backend: "foo:80",
	})
	req = req.WithContext(ctx)

	assert.NoError(err)
	rec := httptest.NewRecorder()

	proxyHandler.Handle(rec, req)
	res := rec.Result()
	assert.Equal(http.StatusOK, res.StatusCode)
	// Check that the TestH header is forwarded
	assert.Equal("foo", res.Header.Get("TestH"))
	// Verify that the X-Request-Id is NOT forwarded to the end-client
	assert.Equal("", res.Header.Get(golibs.HeaderRequestIDName))

	// check the body is correctly forwarded
	body, err := ioutil.ReadAll(res.Body)
	assert.Nil(err)
	assert.Equal(bodyTestText, string(body))
}

func TestProxyHandlerWithClientError(t *testing.T) {
	assert := assert.New(t)

	logger := logrus.WithFields(logrus.Fields{})
	// Mute the logger
	logger.Logger.Out = ioutil.Discard

	proxyHandler := NewProxyHandler(logger, getMockClient(func(*http.Request) (*http.Response, error) {
		return &http.Response{}, fmt.Errorf("error")
	}))
	req, err := http.NewRequest(http.MethodGet, "dummy", nil)
	ctx := req.Context()
	ctx = context.WithValue(ctx, ServiceContextKey, &Service{
		Name:    "foo",
		Backend: "foo:80",
	})
	req = req.WithContext(ctx)

	assert.NoError(err)
	rec := httptest.NewRecorder()

	proxyHandler.Handle(rec, req)

	// Check for Bad Request
	assert.Equal(http.StatusBadGateway, rec.Result().StatusCode)
}

func TestProxyHandlerWithBody(t *testing.T) {
	assert := assert.New(t)

	logger := logrus.WithFields(logrus.Fields{})
	// Mute the logger
	logger.Logger.Out = ioutil.Discard

	proxyHandler := NewProxyHandler(logger, getMockClient(func(req *http.Request) (*http.Response, error) {
		body, err := ioutil.ReadAll(req.Body)

		assert.Nil(err)
		assert.Equal("foo bar", string(body))

		return &http.Response{}, nil
	}))

	bodyText := "foo bar"
	body := ioutil.NopCloser(bytes.NewBufferString(bodyText))
	req, err := http.NewRequest(http.MethodPost, "dummy", body)
	req.Header = http.Header{
		"Test-Header": []string{"foo", "bar"},
	}
	ctx := req.Context()
	ctx = context.WithValue(ctx, ServiceContextKey, &Service{
		Name:    "foo",
		Backend: "foo:80",
	})
	req = req.WithContext(ctx)

	assert.NoError(err)
	rec := httptest.NewRecorder()

	proxyHandler.Handle(rec, req)
	res := rec.Result()

	assert.Equal(http.StatusOK, res.StatusCode)
}

type mockClient struct {
	DoFunc func(*http.Request) (*http.Response, error)
}

func (m *mockClient) Do(req *http.Request) (*http.Response, error) {
	return m.DoFunc(req)
}

func getMockClient(doFunc func(*http.Request) (*http.Response, error)) *mockClient {
	newClient := &mockClient{
		DoFunc: doFunc,
	}

	return newClient
}
