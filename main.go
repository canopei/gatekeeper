package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"strings"
	"time"

	"bitbucket.org/canopei/golibs/healthcheck"
	cHttp "bitbucket.org/canopei/golibs/http"
	"bitbucket.org/canopei/golibs/logging"

	"github.com/Sirupsen/logrus"
	"github.com/gorilla/mux"
	"github.com/urfave/negroni"
)

var (
	conf    *Config
	logger  *logrus.Entry
	version string
)

func init() {
	var err error

	configFile := flag.String("config", "config.toml", "the path to the config file")
	flag.Parse()

	envConfigFile := os.Getenv("GATEKEEPER_CONFIG_FILE")
	if envConfigFile != "" {
		configFile = &envConfigFile
	}

	if conf, err = LoadConfig(*configFile); err != nil {
		logrus.WithFields(nil).Fatalf("Unable to read the config file: %v", err)
	}
}

func main() {
	// This actually connects to logstash - so we cannot place it in init()
	logger = logging.GetLogstashLogger(conf.Service.Env, conf.Service.Name, &conf.Logstash, logrus.Fields{})

	// Read the version from the disk
	b, err := ioutil.ReadFile("VERSION")
	if err != nil {
		logger.Fatalf("Cannot read the version file: %v", err)
	}
	version = strings.TrimSpace(string(b))

	logger.Infof("Booting '%s' (%s)...", conf.Service.Name, version)

	r := NewRouter(logger)

	s := &http.Server{
		Addr:           fmt.Sprintf(":%d", conf.Service.Port),
		Handler:        r,
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: 1 << 20,
	}

	logger.Infof("Serving on %d.", conf.Service.Port)
	logger.Panicf("%v", s.ListenAndServe())
}

// NewRouter creates our router
func NewRouter(logger *logrus.Entry) http.Handler {
	externalRouter := mux.NewRouter()
	internalRouter := mux.NewRouter().PathPrefix("/").Subrouter().StrictSlash(true)

	// Make this available always, e.g. for kubernetes health checks
	externalRouter.HandleFunc(healthcheck.Healthpath, HealthcheckHandler)

	// Middlewares
	n := negroni.New()
	n.Use(cHttp.NewXRequestIDMiddleware(16))
	n.Use(NewLoggingMiddlware(logger, LoggerContextKey))
	n.Use(NewServiceMiddleware(logger, conf.Service.ExternalApex, conf.Services, LoggerContextKey, ServiceContextKey))
	n.Use(negroni.Wrap(internalRouter))
	externalRouter.PathPrefix("/").Handler(n)

	client := &http.Client{
		// RETURN a redirect, do not FOLLOW it (which ends up causing relative redirect issues)
		CheckRedirect: func(req *http.Request, via []*http.Request) error {
			return http.ErrUseLastResponse
		},
	}
	proxyHandler := NewProxyHandler(logger, client)
	internalRouter.PathPrefix("/").HandlerFunc(proxyHandler.Handle)

	return externalRouter
}

// HealthcheckHandler handle the healthcheck request
func HealthcheckHandler(res http.ResponseWriter, req *http.Request) {
	res.WriteHeader(http.StatusOK)
	res.Header().Set("Content-Type", "application/json")
	msg, _ := json.Marshal(map[string]string{"status": "ok", "version": version})
	res.Write([]byte(msg))
	return
}
