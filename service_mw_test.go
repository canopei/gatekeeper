package main

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/Sirupsen/logrus"
	"github.com/stretchr/testify/assert"
)

func TestServeHTTP(t *testing.T) {
	assert := assert.New(t)

	services := ServiceDirectory{
		"foo": &Service{
			Name:    "foo",
			Backend: "foo:8080",
		},
		"bar": &Service{
			Name:    "bar",
			Backend: "bar:8080",
		},
	}
	logger := logrus.WithFields(logrus.Fields{})
	// Mute the logger
	// logger.Logger.Out = ioutil.Discard

	tests := []struct {
		URL                        string
		IsServiceExpectedInContext bool
	}{
		{"http://foo.sng.com", true},
		{"https://bAr.sNg.COM", true},
		{"http://rand.sng.com", false},
		{"http://nope.sng.co.com", false},
	}

	mw := NewServiceMiddleware(logger, "sng.com", services, LoggerContextKey, ServiceContextKey)

	// Used to ensure that next is called
	nextCalled := false
	isServiceExpectedInContext := false
	next := func(res http.ResponseWriter, req *http.Request) {
		nextCalled = true

		// Check that we have the service in the context, or not
		contextService := req.Context().Value(ServiceContextKey)
		assert.Equal(isServiceExpectedInContext, contextService != nil)
	}

	methods := []string{http.MethodGet, http.MethodPost, http.MethodDelete}

	for _, method := range methods {
		for _, test := range tests {
			nextCalled = false
			isServiceExpectedInContext = test.IsServiceExpectedInContext

			req, err := http.NewRequest(method, test.URL, nil)
			assert.NoError(err)
			rec := httptest.NewRecorder()

			mw.ServeHTTP(rec, req, next)

			// next should be always called
			assert.True(nextCalled)
		}
	}
}

func TestGetSubdomainFromHost(t *testing.T) {
	assert := assert.New(t)

	apex := "sng.com"

	noErrTests := map[string]string{
		"foo.sng.com":          "foo",
		"a.b.sNg.com":          "a.b",
		"staffjoy.com.sng.COM": "staffjoy.com",
		"feynman.sng.com:80":   "feynman",
	}
	for host, expected := range noErrTests {
		actual, err := GetSubdomainFromHost(host, apex)
		assert.Nil(err)
		assert.Equal(actual, expected)
	}

	// These tests should throw an error
	errTests := []string{
		"foo.staffjoy.co",
		"foo.staffjoy.com.co",
		"feynman",
		"staffjoy.com",
		"moc.yojffats.sdrawkcab",
		"feynmanstaffjoy.com",
		"",
	}
	for _, host := range errTests {
		_, err := GetSubdomainFromHost(host, apex)
		assert.NotNil(err)
	}
}
